﻿class Triangle : IShape
{

    private double side1;
    private double side2;
    private double side3;

    public Triangle(double side1, double side2, double side3)
    {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public double CalculateArea()
    {
        // Using Heron's formula
        double p = (side1 + side2 + side3) / 2;//half perimeter of triangle p=(a+b+c)/2
        return Math.Sqrt(p * (p - side1) * (p - side2) * (p - side3));//S = sprt(p x (p – a) x (p – b) x ( p – c))
    }

    public double CalculatePerimeter()
    {
        return side1 + side2 + side3;
    }
}
