﻿using System;

class Program
{
    static void Main(string[] args)
    {
		try
		{
            IShape[] shapes = new IShape[9];

            shapes[0] = new Rectangle(5, 10);
            shapes[1] = new Rectangle(3, 7);
            shapes[2] = new Rectangle(4, 6);

            shapes[3] = new Triangle(5, 2, 3);
            shapes[4] = new Triangle(7, 4, 5);
            shapes[5] = new Triangle(9, 4, 2);

            shapes[6] = new Circle(5);
            shapes[7] = new Circle(7);
            shapes[8] = new Circle(9);

            for (int i = 0; i < shapes.Length; i++)
            {
                Console.WriteLine("The area of the " + shapes[i].GetType().Name + " is " + shapes[i].CalculateArea());
                Console.WriteLine("The perimeter of the " + shapes[i].GetType().Name + " is " + shapes[i].CalculatePerimeter());
                Console.WriteLine();
            }

        }
        catch (Exception e)
		{

            Console.WriteLine(e.Message);
        }
        Console.ReadLine();
     }
}
